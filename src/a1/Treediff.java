package a1;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Treediff {

    public static void main(String[] args) throws IOException, NoSuchAlgorithmException {

        boolean isTrue = false;

        Path pathA = Paths.get("/tmp/dirA");
        Path pathB = Paths.get("/tmp/dirB");

        List<Path> pathsA = Files.walk(pathA).filter(Files::isRegularFile).collect(Collectors.toList());
        List<Path> pathsB = Files.walk(pathB).filter(Files::isRegularFile).collect(Collectors.toList());

        System.out.println("\n\n 1. Els fitxers del DirectoriA que no estan al DirectoriB");
        System.out.println("-------------------------------------------------------------------------------");

        // 1. Els fitxers del DirectoriA que no estan al DirectoriB

        for (Path file:pathsA) {
            byte[] hash1 = MessageDigest.getInstance("MD5").digest(Files.readAllBytes(file));
            isTrue = false;
            for (Path file2:pathsB) {
                byte[] hash2 = MessageDigest.getInstance("MD5").digest(Files.readAllBytes(file2));
                if (Arrays.equals(hash1, hash2)) {    isTrue = true;    }
            }
            if (!isTrue == true) { System.out.println("El fitxer " + file.getFileName() + " del directoriA no esta al directoriB"); }
        }

        System.out.println("\n\n\n 2. Els fitxers del DirectoriA que estan al DirectoriB però en una altra ruta");
        System.out.println("-------------------------------------------------------------------------------");

        // 2. Els fitxers del DirectoriA que estan al DirectoriB però en una altra ruta

        for (Path file:pathsA) {
            byte[] hash1 = MessageDigest.getInstance("MD5").digest(Files.readAllBytes(file));
            isTrue = false;
            for (Path file2:pathsB) {
                byte[] hash2 = MessageDigest.getInstance("MD5").digest(Files.readAllBytes(file2));
                if (Arrays.equals(hash1, hash2)) {
                    if (!pathA.relativize(file).equals(pathB.relativize(file2))) {
                        isTrue = true;
                    }
                }
            }
            if (isTrue == true) { System.out.println("El fitxer " + file.getFileName() + " del directoriA esta al directoriB pero en una altra ruta"); }
        }

        System.out.println("\n\n\n 3. Els fitxers del DirectoriB que no estan al DirectoriA");
        System.out.println("-------------------------------------------------------------------------------");

        // 3. Els fitxers del DirectoriB que no estan al DirectoriA

        for (Path file:pathsB) {
            byte[] hash1 = MessageDigest.getInstance("MD5").digest(Files.readAllBytes(file));
            isTrue = false;
            for (Path file2:pathsA) {
                byte[] hash2 = MessageDigest.getInstance("MD5").digest(Files.readAllBytes(file2));
                if (Arrays.equals(hash1, hash2)) {    isTrue = true;    }
            }
            if (!isTrue == true) { System.out.println("El fitxer " + file.getFileName() + " del directoriB no esta al directoriA"); }
        }

        System.out.println("\n\n\n 4. Els fitxers del DirectoriB que estan al DirectoriA però en una altra ruta");
        System.out.println("-------------------------------------------------------------------------------");

        // 4. Els fitxers del DirectoriB que estan al DirectoriA però en una altra ruta

        for (Path file:pathsB) {
            byte[] hash1 = MessageDigest.getInstance("MD5").digest(Files.readAllBytes(file));
            isTrue = false;
            for (Path file2:pathsA) {
                byte[] hash2 = MessageDigest.getInstance("MD5").digest(Files.readAllBytes(file2));
                if (Arrays.equals(hash1, hash2)) {
                    if (!pathB.relativize(file).equals(pathA.relativize(file2))) {
                        isTrue = true;
                    }
                }
            }
            if (isTrue == true) { System.out.println("El fitxer " + file.getFileName() + " del directoriB esta al directoriA pero en una altra ruta"); }
        }

        System.out.println("\n\n\n 5. Els fitxers que estan a la mateixa ruta al DirectoriA i al DirectoriB");
        System.out.println("-------------------------------------------------------------------------------");

        // 5. Els fitxers que estan a la mateixa ruta al DirectoriA i al DirectoriB

        System.out.println("\n5. Els fitxers que estan a la mateixa ruta al DirectoriA i al DirectoriB");
        for (Path file:pathsA) {
            byte[] hash1 = MessageDigest.getInstance("MD5").digest(Files.readAllBytes(file));
            isTrue = false;
            for (Path file2:pathsB) {
                byte[] hash2 = MessageDigest.getInstance("MD5").digest(Files.readAllBytes(file2));
                if (Arrays.equals(hash1, hash2)) {
                    if (pathA.relativize(file).equals(pathB.relativize(file2))) {
                        isTrue = true;
                    }
                }
            }
            if (isTrue == true) { System.out.println("El fitxer " + file.getFileName() + " del directoriA esta al directoriB i a la mateixa ruta"); }
        }
    }
}