package a1;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;

public class DateTree {

    public static void main(String[] args) throws IOException {

        Path dir = Paths.get("/tmp/directori");
        Files.walk(dir).filter(Files::isRegularFile).forEach(
                file -> {
                    try {
                        LocalDateTime date = LocalDateTime.parse(Files.getLastModifiedTime(file).toString(), DateTimeFormatter.ISO_DATE_TIME);

                        String newDir = "/tmp/directori" + "/" +date.getYear() + "/" + date.getMonthValue()+"/" + date.getDayOfMonth();
                        Path newPath = Paths.get(newDir);
                        Files.createDirectories(newPath);
                        Files.move(file, newPath.resolve(file.getFileName()));

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
        );

        Files.walk(dir).filter(Files::isDirectory).sorted(Collections.reverseOrder()).forEach(
                file -> {
                    try {
                        if (!Files.list(file).findAny().isPresent()){
                            Files.delete(file);
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
        );

    }
}