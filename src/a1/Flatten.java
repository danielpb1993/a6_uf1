package a1;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;

public class Flatten {
    public static void main(String[] args) throws IOException {
        Path dir = Paths.get("/tmp/flatten");

        Files.walk(dir)
                .filter(Files::isRegularFile)
                .forEach(file -> {
                    try {
                        Files.move(file, dir.resolve(file.getFileName()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
        Files.walk(dir)
                .filter(Files::isDirectory)
                .sorted(Comparator.reverseOrder())
                .forEach(file -> {
                    if ((dir != file)) {
                        try {
                            Files.delete(file);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }
}
