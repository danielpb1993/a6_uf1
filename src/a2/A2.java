package a2;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class A2 {

    public static void Split(String pArxiu, int parts) throws IOException {
        Path arxiu = Paths.get(pArxiu);

        int cont = 0;
        Path splittedFile = Paths.get(arxiu + ".part." +  cont);

        InputStream is = Files.newInputStream(arxiu);
        int newSplittedSize = (int) Math.ceil((double)is.available()/parts);
        OutputStream os = Files.newOutputStream(splittedFile);

        double bys = 0;

        for (int a;  (a = is.read()) != -1;) {

            if (bys == newSplittedSize) {
                cont++;
                splittedFile = Paths.get(arxiu + ".part." +  cont);
                bys = 0;
                os = Files.newOutputStream(splittedFile);
            }
            bys++;
            os.write(a);
        }
    }

    public static void Join(String path) throws IOException {

        Path outfile = Paths.get(path);
        OutputStream os = Files.newOutputStream(outfile);

        for (int i = 0; ; i++) {
            Path infile = Paths.get(outfile + ".part." + i);
            if (infile.toFile().isFile()) {
                InputStream is = Files.newInputStream(infile);
                for (int a; (a = is.read()) != -1;) {
                    os.write(a);
                }
            }
        }
    }



    public static void main(String[] args) throws IOException {
        Split("/tmp/a2.pdf", 5);
        Join("/tmp/a2.pdf");
    }

}