package a3;

/**
 * @author Daniel Penarroya
 *
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.Scanner;

public class A3 {

    static String file = "/tmp/scores.txt";

    public static void main(String[] args) throws IOException {
        BufferedReader br =  Files.newBufferedReader(Paths.get(file));
        Scanner sc = new Scanner(System.in);

        List<Player> players = readPlayers(br);

        System.out.println("Enter player: ");
        String name = sc.nextLine();

        System.out.println("Enter score: ");
        int score = sc.nextInt();
        players.add(new Player(name, score));

        writePlayers(file,players);
        br =  Files.newBufferedReader(Paths.get(file));
        players = readPlayers(br);
        top5(players);
        sc.close();
    }

    private static List<Player> readPlayers (BufferedReader br) throws IOException {
        String line = null;
        List<Player> players = new ArrayList<>();

        while ((line = br.readLine()) != null) {
            String[] values = line.split(",");
            String name = values[0];
            String point = values[1];
            Player player = new Player(name, Integer.parseInt(point));
            players.add(player);
        }
        br.close();

        return players;
    }

    private static void writePlayers (String file, List<Player> players) throws IOException {
        List<String> lineas = new ArrayList<>();

        players = players.stream()
                .sorted(Comparator.comparing(Player::getPlayerPoint).reversed())
                .limit(5)
                .collect(Collectors.toList());

        for (Player player:players) {
            String linea = player.getPlayerName() + "," + player.playerPoint;
            lineas.add(linea);
        }
        Files.write(Paths.get(file), lineas);
    }

    private static void top5 (List<Player> players){
        System.out.println("---------- TOP 5 ------------");
        for (int i = 0; i < players.size(); i++) {
            System.out.println(players.get(i));
        }
    }
}