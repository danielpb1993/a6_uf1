package a3;

import java.io.File;
import java.util.Date;

public class Ex {
    public static void main(String[] args) {
        pregunta1("/tmp/directori");
    }
    public static void pregunta1(String path) {
        File carpeta = new File(path);

        for(File file: carpeta.listFiles()) {
            System.out.println("Name: " + file.getName());
            Date d = new Date(file.lastModified());
            System.out.println("Last modification: " + d);
        }
    }
}
